$(document).ready(function() {
$( "#showItems" ).click(function() {
	
	var url = "../getItems?q=" 
		+ $("#champion1Field").val() + "," + $("#champion2Field").val() + "," + $("#position").val();
	$.getJSON(url, function(data) {
		
		var html = "";
		console.log(data);
		
		var index = 0;
		//array of {percentage,htmlString}
		console.log("number of matches is: " + data[0].NumMatches);
		var myArray = [];
		for (var item in data[0].Items)
			{
				console.log(data[0].Items[item].Amount);
				if (data[0].Items[item].Amount != 0)
				{
					
					var percentage = ((data[0].Items[item].Amount/data[0].NumMatches) * 100).toFixed(2);
					percentage = percentage.toString();
					
					var myString = ("<div class = \"item\"><img class = \"itemImage\" src = \"" + "//ddragon.leagueoflegends.com/cdn/6.7.1/img/item/" + data[0].Items[item].ID + ".png" + "\">"  + "<div class = \"itemName\">" + item + "</div>" + "<div class = \"percentage\">" + percentage + "%" + "</div>" + "</div>");
					if (percentage.length == 4)
						percentage = "0" + percentage;
					console.log(myString);
					if (myArray.length == 0)
					{	
						console.log("still working");
						myArray[0] = {percent:percentage, htmlString:myString};
						console.log("got past it");

					}
					else
					{
						for (i = 0; i < myArray.length; i++)
						{
							if (percentage > myArray[i].percent)
							{
								console.log("before splice");
								myArray.splice(i, 0, {percent:percentage, htmlString:myString});
								console.log("after splice");
								break;

							}
						}
					}
				}
			}
		console.log("arrray length " + myArray.length);
		for (i = 0; i < myArray.length; i++)
		{
			html += myArray[i].htmlString;
		}
		$("#ItemList").html(html);

	})
})
});
