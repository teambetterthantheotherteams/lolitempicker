
function InsertText(message)
{
	document.getElementById("champion1Field").value = message;
	
	$("#chmp1Hint").html("");
}

function InsertText2(message)
{
	document.getElementById("champion2Field").value = message;
	
	$("#chmp2Hint").html("");

}

$(document).ready(function() {
$( "#champion1Field" ).keyup(function() {
  if ($("#champion1Field").val() == "") return;
   	var url = "../getChampionName?q="+$("#champion1Field").val();
  $.getJSON(url,function(data) {
  var myRe = new RegExp("^" + $("#champion1Field").val());
    var everything;
  everything = "";
  $.each(data, function(i,item) {
    if (data[i].Name.search(myRe) != -1)
    {
          everything += ("<button class =\"champButton\" type = \"button\" onclick = \"InsertText('" + data[i].Name + "')\"><img class = \"champButtonImage\" src = \"" +data[i].ImageURL + "\" height = \"45\" width = \"45\">" +data[i].Name + "<br></button>");

    }
  });
    
  everything += "</ul>";
  $("#chmp1Hint").html(everything);
  });
  
});
});

$(document).ready(function() {
$( "#champion2Field" ).keyup(function() {
  if ($("#champion2Field").val() == "") return;
   	var url = "../getChampionName?q="+$("#champion2Field").val();
  $.getJSON(url,function(data) {
  var myRe = new RegExp("^" + $("#champion2Field").val());
    var everything;
  everything = "";
  $.each(data, function(i,item) {
    if (data[i].Name.search(myRe) != -1)
    {
          everything += ("<button class =\"champButton\" type = \"button\" onclick = \"InsertText2('" + data[i].Name + "')\"><img class = \"champButtonImage\" src = \"" +data[i].ImageURL + "\" height = \"45\" width = \"45\">" +data[i].Name + "<br></button>");
    }
  });
    
  everything += "</ul>";
  $("#chmp2Hint").html(everything);
  });
  
});
});
