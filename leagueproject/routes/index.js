var express = require('express');

var router = express.Router();
var fs = require('fs');
var mongoose = require('mongoose');



mongoose.connect('mongodb://localhost/leagueDB'); //Connects to a mongo database called "commentDB"

var championsSchema = mongoose.Schema({ //Defines the Schema for this database
  Name: String,
  ImageURL: String
});

var Champion = mongoose.model('Champion',championsSchema);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.sendFile('Project6.html', { root:  'public' });
});


router.post('/champion', function (req, res, next) {
	
	res.sendStatus(200);
	var newChampion = new Champion(req.body);
	newChampion.save(function(err, post) {
		if (err) return console.error(err);
	});
});

router.get('/getChampionName',function(req,res,next) {
            Champion.find(function(err, championList) {
	    if (err) return console.error(err);
	    else {
			res.status(200).json(championList);
	         } 		

             })
          });

router.get('/getItems', function (req,res,next) {
		console.log("the body of the request is: " + req.query.q);
		var all = req.query.q.split(',');
		console.log("0 is: " + all[0]);
		var myPlayingAs = all[0];
		var myPlayingAgainst = all[1];
		var myPosition = all[2];
		var query = {};
		console.log("ok");
		query["_id"] = myPosition + myPlayingAs + myPlayingAgainst;		
				
	    mongoose.connection.db.collection('matchups').find(query).toArray(function(err, data)
		{
			if (err) return console.error(err)
			else {	
				console.log(data);
				res.send(data);
			}
		})
	   });
	    

module.exports = router;
