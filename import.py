import json
import urllib2
import time
from pymongo import MongoClient

secondsPerRequest = 2
lanes = ['TOP', 'BOTTOM', 'JUNGLE', 'MIDDLE']

client = MongoClient()

#while True:
if True:
    thumbnails = urllib2.urlopen("https://global.api.pvp.net/api/lol/static-data/na/v1.2/champion?champData=image&api_key=919fce1d-fcdc-40d9-a8b1-7344db367f8a")
    time.sleep(secondsPerRequest)
    db = client.leagueDB
    champs = db.champions
    result = champs.drop()
    thumbnails = json.load(thumbnails)
    for i in thumbnails['data']:
        print(i)
        imgurl = "http://ddragon.leagueoflegends.com/cdn/6.6.1/img/champion/" + i + ".png"
        print(imgurl)
        response = champs.insert_one(
            {
                "ImageURL": imgurl,
                "_id":thumbnails['data'][i]['id'],
                "Name":i
            }
        )
        print(result)

    blankItems = {}
    items = db.items
    iteminfo = urllib2.urlopen("https://global.api.pvp.net/api/lol/static-data/na/v1.2/item?itemListData=image&api_key=919fce1d-fcdc-40d9-a8b1-7344db367f8a").read()
    time.sleep(secondsPerRequest)
    iteminfo = json.loads(iteminfo)
    result = items.drop()
    for i in iteminfo['data']:
        print(iteminfo['data'][i]['id'])
        imgurl = "http://ddragon.leagueoflegends.com/cdn/" + iteminfo['version']+ "/img/item/" + iteminfo['data'][i]['image']['full']
        print(imgurl)
        try:
            itemname = iteminfo['data'][i]['name']
            itemname = itemname.replace(".", "")
            itemname = itemname.replace(")", "")
            itemname = itemname.replace("(", "")
            itemname = itemname.replace("'", "")
            print(itemname)
            result = items.insert_one(
                {
                    "_id": iteminfo['data'][i]['id'],
                    "Name": itemname
                    #"ImageURL": imgurl
                }
            )
            blankItems[itemname] = {
                #"ImageURL": imgurl,
                "ID": iteminfo['data'][i]['id'],
                "Amount": 0
            }
            print(result)
        except:
            pass
    print(blankItems)

    matchups = db.matchups
    matchups.drop()
    for i in thumbnails['data']:
        for k in thumbnails['data']:
            for j in lanes:
                print(i + " vs " + k + " in " + j)
                result = matchups.insert_one(
                    {   "_id": j + i + k,
                        "Position": j,
                        #"PlayingAs": i,
                        #"PlayingAsId": thumbnails['data'][i]['id'],
                        #"PlayingAgainst": k,
                        #"PlayingAgainstId": thumbnails['data'][k]['id'],
                        "NumMatches": 0,
                        "Items": blankItems
                    }
                )

    ids = urllib2.urlopen("https://na.api.pvp.net/api/lol/na/v2.5/league/challenger?type=RANKED_SOLO_5x5&api_key=919fce1d-fcdc-40d9-a8b1-7344db367f8a").read()
    time.sleep(secondsPerRequest)
    ids = json.loads(ids)
    for i in ids['entries']:
        print(i['playerOrTeamId'])
        url = "https://na.api.pvp.net/api/lol/na/v2.2/matchlist/by-summoner/"
        url += i['playerOrTeamId']
        url += "?api_key=919fce1d-fcdc-40d9-a8b1-7344db367f8a"
        matches = urllib2.urlopen(url)
        time.sleep(secondsPerRequest)
        matches = json.load(matches)
        for k in matches['matches']:
            print(k['matchId'])
            url = "https://na.api.pvp.net/api/lol/na/v2.2/match/"
            url += str(k['matchId'])
            url += "?api_key=919fce1d-fcdc-40d9-a8b1-7344db367f8a"
            match = urllib2.urlopen(url)
            time.sleep(secondsPerRequest)
            match = json.load(match)
            for j in match['participants']:
                laneAs = j['timeline']['lane']
                teamAs = j['teamId']
                for l in match['participants']:
                    laneAgainst = l['timeline']['lane']
                    teamAgainst = l['teamId']
                    if laneAgainst == laneAs and teamAgainst != teamAs:
                        print(str(j['championId']) + " vs " + str(l['championId']) + " at " + laneAs)
                        nameAs = champs.find({'_id': j['championId']})[0]['Name']
                        nameAgainst = champs.find({'_id': l['championId']})[0]['Name']
                        id = laneAs + nameAs + nameAgainst
                        print(id)
                        matchups.update_one({'_id': id}, {'$inc': {'NumMatches':int(1)}})
                        if items.find({'_id': j['stats']['item0']}).count() > 0:
                            item0 = "Items." + items.find({'_id': j['stats']['item0']})[0]['Name'] + '.Amount'
                            print(item0)
                            matchups.update_one({'_id': id}, {'$inc': {item0:int(1)}})
                        if items.find({'_id': j['stats']['item1']}).count() > 0:
                            item1 = "Items." + items.find({'_id': j['stats']['item1']})[0]['Name'] + '.Amount'
                            matchups.update_one({'_id': id}, {'$inc': {item1:int(1)}})
                        if items.find({'_id': j['stats']['item2']}).count() > 0:
                            item2 = "Items." + items.find({'_id': j['stats']['item2']})[0]['Name'] + '.Amount'
                            matchups.update_one({'_id': id}, {'$inc': {item2:int(1)}})
                        if items.find({'_id': j['stats']['item3']}).count() > 0:
                            item3 = "Items." + items.find({'_id': j['stats']['item3']})[0]['Name'] + '.Amount'
                            matchups.update_one({'_id': id}, {'$inc': {item3:int(1)}})
                        if items.find({'_id': j['stats']['item4']}).count() > 0:
                            item4 = "Items." + items.find({'_id': j['stats']['item4']})[0]['Name'] + '.Amount'
                            matchups.update_one({'_id': id}, {'$inc': {item4:int(1)}})
                        if items.find({'_id': j['stats']['item5']}).count() > 0:
                            item5 = "Items." + items.find({'_id': j['stats']['item5']})[0]['Name'] + '.Amount'
                            matchups.update_one({'_id': id}, {'$inc': {item5:int(1)}})
                        if items.find({'_id': j['stats']['item6']}).count() > 0:
                            item6 = "Items." + items.find({'_id': j['stats']['item6']})[0]['Name'] + '.Amount'
                            matchups.update_one({'_id': id}, {'$inc': {item6:int(1)}})
